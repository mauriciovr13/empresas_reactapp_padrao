import React, { useEffect } from 'react';
import { StatusBar } from 'react-native';
import styled from 'styled-components';
import 'react-native-gesture-handler';

/* Navigation*/
import RootNavigator from '@navigation';

// /* Redux */
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import reduxThunk from 'redux-thunk';
import reduxLogger from 'redux-logger';
import reducers from '@reducers/reducers';

/* init store redux */
const middleware = [reduxThunk];

if (__DEV__) {
  middleware.push(reduxLogger);
}

const store = createStore(reducers, compose(applyMiddleware(...middleware)));

const App = () => {
  return (
    <Provider store={store}>
      <StatusBar barStyle={'light-content'} backgroundColor='#000' />
      <RootNavigator />
    </Provider>
  );
};

const SafeAreaViewStyled = styled.SafeAreaView`
  flex: 1;
  justify-content: center;
`;

export default App;
