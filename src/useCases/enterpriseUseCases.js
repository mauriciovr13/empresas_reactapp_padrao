import { Alert } from 'react-native';
import axios from 'axios';
import enterpriseActions from '@actions/enterpriseActions';

import CONSTANTS from '../config/constants';

export const loadEnterprises = () => {
  return (dispatch, getState) => {
    dispatch(enterpriseActions.GET_ENTERPRISES());
    const state = getState();
    const user = state.user;
    axios({
      method: 'GET',
      url: `${CONSTANTS.SERVER}/api/v1/enterprises`,
      headers: {
        'access-token': user.tokens['access-token'],
        client: user.tokens['client'],
        uid: user.tokens['uid']
      }
    })
      .then(response => {
        dispatch(
          enterpriseActions.GET_ENTERPRISES_SUCCESS(response.data.enterprises)
        );
      })
      .catch(error => {
        dispatch(enterpriseActions.GET_ENTERPRISES_FAILURE(error));
        Alert.alert('Erro ao buscar as empresas. ' + error);
      });
  };
};

export const filterEnterprises = (enterpriseTypes, name) => {
  return (dispatch, getState) => {
    console.log(enterpriseTypes, name);
    dispatch(enterpriseActions.GET_ENTERPRISES_WITH_FILTER());
    const state = getState();
    const user = state.user;
    const type =
      enterpriseTypes !== 0 ? `enterpriseType=${enterpriseTypes}&` : '';
    axios({
      method: 'GET',
      url: `${CONSTANTS.SERVER}/api/v1/enterprises?${type}name=${name}`,
      headers: {
        'access-token': user.tokens['access-token'],
        client: user.tokens['client'],
        uid: user.tokens['uid']
      }
    })
      .then(response => {
        dispatch(
          enterpriseActions.GET_ENTERPRISES_WITH_FILTER_SUCCESS(
            response.data.enterprises
          )
        );
      })
      .catch(error => {
        dispatch(enterpriseActions.GET_ENTERPRISES_WITH_FILTER_FAILURE(error));
        Alert.alert('Erro ao filtrar as empresas. ' + error);
      });
  };
};

export const enterpriseDetails = enterprise_id => {
  return (dispatch, getState) => {
    dispatch(enterpriseActions.GET_ENTERPRISE_DETAILS());
    const state = getState();
    const user = state.user;
    axios({
      method: 'GET',
      url: `${CONSTANTS.SERVER}/api/v1/enterprises/${enterprise_id}`,
      headers: {
        'access-token': user.tokens['access-token'],
        client: user.tokens['client'],
        uid: user.tokens['uid']
      }
    })
      .then(response => {
        dispatch(
          enterpriseActions.GET_ENTERPRISE_DETAILS_SUCCESS(
            response.data.enterprise
          )
        );
      })
      .catch(error => {
        dispatch(enterpriseActions.GET_ENTERPRISE_DETAILS_FAILURE(error));
        Alert.alert('Erro ao detalhar uma empresa. ' + error);
      });
  };
};
