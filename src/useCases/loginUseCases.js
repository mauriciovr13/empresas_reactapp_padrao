import { Alert } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import userActions from '@actions/userActions';
import axios from 'axios';

import CONSTANTS from '../config/constants';

export const signIn = (email, password, callback = () => {}) => {
  return dispatch => {
    dispatch(userActions.USER_LOGIN());
    axios({
      method: 'POST',
      url: `${CONSTANTS.SERVER}/api/v1/users/auth/sign_in`,
      data: {
        email: email,
        password: password
      }
    })
      .then(response => {
        const user = {
          'access-token': response.headers['access-token'],
          client: response.headers['client'],
          uid: response.headers['uid']
        };
        AsyncStorage.setItem('user', JSON.stringify(user));
        dispatch(userActions.USER_LOGIN_SUCCESS(user));
        callback();
      })
      .catch(error => {
        dispatch(userActions.USER_LOGIN_FAILURE(error));
        Alert.alert('Verifique a senha e/ou usuário informado(s). ' + error);
      });
  };
};

export const logout = () => {
  return dispatch => {
    dispatch(userActions.USER_LOGOUT());
    // AsyncStorage.removeItem('user', () => Alert.alert('Erro ao fazer logout'));
  };
};
