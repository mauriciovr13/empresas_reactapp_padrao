export default {
  USER_LOGIN: 'USER_LOGIN',
  USER_LOGIN_SUCCESS: 'USER_LOGIN_SUCCESS',
  USER_LOGIN_FAILURE: 'USER_LOGIN_FAILURE',

  USER_LOGOUT: 'USER_LOGOUT',

  GET_ENTERPRISES: 'GET_ENTERPRISES',
  GET_ENTERPRISES_SUCCESS: 'GET_ENTERPRISES_SUCCESS',
  GET_ENTERPRISES_FAILURE: 'GET_ENTERPRISES_FAILURE',
  GET_ENTERPRISES_WITH_FILTER: 'GET_ENTERPRISES_WITH_FILTER',
  GET_ENTERPRISES_WITH_FILTER_SUCCESS: 'GET_ENTERPRISES_WITH_FILTER_SUCCESS',
  GET_ENTERPRISES_WITH_FILTER_FAILURE: 'GET_ENTERPRISES_WITH_FILTER_FAILURE',

  GET_ENTERPRISE_DETAILS: 'GET_ENTERPRISE_DETAILS',
  GET_ENTERPRISE_DETAILS_SUCCESS: 'GET_ENTERPRISE_DETAILS_SUCCESS',
  GET_ENTERPRISE_DETAILS_FAILURE: 'GET_ENTERPRISE_DETAILS_FAILURE'
};
