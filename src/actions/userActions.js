import actions from './actions';

function USER_LOGIN() {
  return {
    type: actions.USER_LOGIN
  };
}
function USER_LOGIN_SUCCESS(data) {
  return {
    type: actions.USER_LOGIN_SUCCESS,
    payload: {
      data
    }
  };
}
function USER_LOGIN_FAILURE(error) {
  return {
    type: actions.USER_LOGIN_FAILURE,
    payload: {
      error
    }
  };
}
function USER_LOGOUT() {
  return {
    type: actions.USER_LOGOUT
  };
}

const userActions = {
  USER_LOGIN,
  USER_LOGIN_SUCCESS,
  USER_LOGIN_FAILURE,
  USER_LOGOUT
};

export default userActions;
