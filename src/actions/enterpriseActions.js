import actions from './actions';

function GET_ENTERPRISES() {
  return {
    type: actions.GET_ENTERPRISES,
  };
}

function GET_ENTERPRISES_SUCCESS(data) {
  return {
    type: actions.GET_ENTERPRISES_SUCCESS,
    payload: {
      data
    }
  };
}

function GET_ENTERPRISES_FAILURE(error) {
  return {
    type: actions.GET_ENTERPRISES_FAILURE,
    payload: {
      error
    }
  };
}

function GET_ENTERPRISES_WITH_FILTER(){
  return {
    type: actions.GET_ENTERPRISES_WITH_FILTER,
  }
}

function GET_ENTERPRISES_WITH_FILTER_SUCCESS(data){
  return {
    type: actions.GET_ENTERPRISES_WITH_FILTER_SUCCESS,
    payload: {
      data
    }
  }
}

function GET_ENTERPRISES_WITH_FILTER_FAILURE(error){
  return {
    type: actions.GET_ENTERPRISES_WITH_FILTER_FAILURE,
    payload: {
      error
    }
  }
}

function GET_ENTERPRISE_DETAILS(){
  return {
    type: actions.GET_ENTERPRISE_DETAILS,
  }
}

function GET_ENTERPRISE_DETAILS_SUCCESS(data){
  return {
    type: actions.GET_ENTERPRISE_DETAILS_SUCCESS,
    payload: {
      data,
    }
  }
}

function GET_ENTERPRISE_DETAILS_FAILURE(error){
  return {
    type: actions.GET_ENTERPRISE_DETAILS_FAILURE,
    payload: {
      error
    }
  }
}

const enterpriseActions = {
  GET_ENTERPRISES,
  GET_ENTERPRISES_SUCCESS,
  GET_ENTERPRISES_FAILURE,
  GET_ENTERPRISES_WITH_FILTER,
  GET_ENTERPRISES_WITH_FILTER_SUCCESS,
  GET_ENTERPRISES_WITH_FILTER_FAILURE,
  GET_ENTERPRISE_DETAILS,
  GET_ENTERPRISE_DETAILS_SUCCESS,
  GET_ENTERPRISE_DETAILS_FAILURE,
}

export default enterpriseActions;