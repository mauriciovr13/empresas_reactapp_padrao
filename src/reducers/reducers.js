import { combineReducers } from 'redux';

/* Reducers */
import user from './userReducer';
import enterprise from './enterpriseReducer';

export default combineReducers({
  user,
  enterprise,
});