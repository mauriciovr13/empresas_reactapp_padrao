import Actions from '@actions/actions';

const defaultState = {
  isLoading: false,
  tokens: undefined
};

export default function userReducer(state = defaultState, action = null) {
  switch (action.type) {
    case Actions.USER_LOGIN:
      return {
        ...state,
        isLoading: true
      };
    case Actions.USER_LOGIN_SUCCESS:
      return {
        ...state,
        tokens: action.payload.data,
        isLoading: false
      };
    case Actions.USER_LOGIN_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: action.payload.error
      };
    case Actions.USER_LOGOUT:
      return defaultState;
    default:
      return state;
  }
}
