import Actions from '@actions/actions';

const defaultState = {
  enterprisesList: [],
  isLoading: false,
  error: undefined,
  enterpriseByID: {},
  refreshing: false
};

export default function enterpriseReducer(state = defaultState, action = null) {
  switch (action.type) {
    case Actions.GET_ENTERPRISES:
    case Actions.GET_ENTERPRISE_DETAILS:
    case Actions.GET_ENTERPRISES_WITH_FILTER:
      return {
        ...state,
        isLoading: true,
        refreshing: true
      };
    case Actions.GET_ENTERPRISES_SUCCESS:
      return {
        ...state,
        enterprisesList: action.payload.data,
        isLoading: false,
        enterpriseByID: {},
        refreshing: false
      };
    case Actions.GET_ENTERPRISES_FAILURE:
      return {
        ...state,
        error: action.payload.error,
        refreshing: false,
        isLoading: false
      };
    case Actions.GET_ENTERPRISES_WITH_FILTER_SUCCESS:
      return {
        ...state,
        enterprisesList: action.payload.data,
        isLoading: false,
        refreshing: false
      };
    case Actions.GET_ENTERPRISES_WITH_FILTER_FAILURE:
      return {
        ...state,
        enterprisesList: [],
        error: action.payload.error,
        isLoading: false,
        refreshing: false
      };
    case Actions.GET_ENTERPRISE_DETAILS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        enterpriseByID: action.payload.data,
        refreshing: false
      };
    case Actions.GET_ENTERPRISE_DETAILS_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: action.payload.error,
        isLoading: false
      };
    default:
      return state;
  }
}
