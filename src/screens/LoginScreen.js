import React from 'react';
import { connect } from 'react-redux';

import Login from '@components/presentational/Login';
import { signIn } from '@useCases/loginUseCases';

const LoginScreen = props => {
  return (
    <>
      <Login
        signIn={props.signIn}
        isLoading={props.isLoading}
        navigation={props.navigation}
      />
    </>
  );
};

const mapDispatchToProps = dispatch => ({
  signIn: (email, password, successCallback) =>
    dispatch(signIn(email, password, successCallback))
});

const mapStateToProps = state => ({
  user: state.user,
  isLoading: state.user.isLoading
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
