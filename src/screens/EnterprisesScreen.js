import React from 'react';
import { connect } from 'react-redux';
import { StackActions, NavigationActions } from 'react-navigation';

import Enterprises from '@components/presentational/Enterprises';
import {
  loadEnterprises,
  filterEnterprises
} from '@useCases/enterpriseUseCases';

import { logout } from '@useCases/loginUseCases';

const EnterprisesScreen = props => {
  console.log(props);
  return (
    <>
      <Enterprises
        enterprises={props.enterprisesList}
        isLoading={props.isLoading}
        refreshing={props.refreshing}
        loadEnterprises={props.loadEnterprises}
        filterEnterprises={props.filterEnterprises}
        navigation={props.navigation}
        logout={props.logout}
      />
    </>
  );
};

const mapDispatchToProps = dispatch => ({
  loadEnterprises: () => dispatch(loadEnterprises()),
  filterEnterprises: (enterpriseTypes, name) =>
    dispatch(filterEnterprises(enterpriseTypes, name)),
  logout: () => dispatch(logout())
});

const mapStateToProps = state => ({
  isLoading: state.enterprise.isLoading,
  enterprisesList: state.enterprise.enterprisesList,
  refreshing: state.enterprise.refreshing
});

export default connect(mapStateToProps, mapDispatchToProps)(EnterprisesScreen);
