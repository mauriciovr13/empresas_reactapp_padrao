import React from 'react';
import { connect } from 'react-redux';
import EnterpriseDetails from '@components/presentational/EnterpriseDetails';
import { enterpriseDetails } from '@useCases/enterpriseUseCases';

const EnterpriseDetailsScreen = props => {
  return (
    <>
      <EnterpriseDetails {...props} />
      {/* enterprise={props.enterprise}
      isLoading={props.isLoading}  
      enterpriseDetails={props.enterpriseDetails}
      navigation={props.navigation} 
    /> */}
    </>
  );
};

const mapDispatchToProps = dispatch => ({
  enterpriseDetails: enterprise_id => dispatch(enterpriseDetails(enterprise_id))
});

const mapStateToProps = state => ({
  enterprise: state.enterprise.enterpriseByID,
  isLoading: state.enterprise.isLoading
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EnterpriseDetailsScreen);
