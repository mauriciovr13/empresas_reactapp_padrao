import React from 'react';

// Fazer os import das screens aqui
import LoginScreen from '@screens/LoginScreen';
import EnterprisesScreen from '@screens/EnterprisesScreen';
import EnterpriseDetailsScreen from '@screens/EnterpriseDetailsScreen';

const routes = {
  Login: LoginScreen,  
  Main: EnterprisesScreen,
  Details: EnterpriseDetailsScreen,
};

export default routes;
  