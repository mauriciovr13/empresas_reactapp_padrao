const COLORS = {
    /* Primary Colors */
    black: '#000000',
    white: '#FFFFFF',
    primary: '#00793A',
    primaryWithoutAlpha: 'rgba(0,121,58,.3)',
    darkerPrimary: '#005629',
    details: '#EDEFEF',
    orange: '#f85d44',
    deepPink:  '#FF1493',
  
    /* Background Colors */
    background: '#FEFEFE',
    lightBackground: '#f8f9fa',
    darkerBackground: '#EDEFEF',
    overlayBackground: 'rgba(0,0,0,0.6)',
    lightOverlayBackground: 'rgba(0,0,0,0.2)',
    grayBackground: '#53545B',
    lightGrayBackground: '#ebebeb',
    blueLightBackground: '#f5f7fa',
    badge: '#ffeee3',
  
    /* Text Colors */
    defaultText: '#4C4C4C',
    primaryText: '#333333',
    secondaryText: '#B2B2B2',
    lightText: '#D0D5DA',
    lightInput: '#dddddd',
    backgroundInput: '#FAFBFC',
    borderInput: '#CCCCCC',
    activeText: '#0076ca',
    inactiveText: '#93ccfd',
    descriptionText: '#666666',
    border: '#E0E0E0',
    search: '#F8F9FA',
    searchBorder: '#E9E9E9',
    disabledText: '#444444',
  
    /* Status Colors */
    successColor: '#5DC0AD',
    alertColor: '#FF4E56',
    infoColor: '#007AFF',
  
    /* Navigation Colors */
    footerNavigation: 'white',
    footerInactiveText: '#53545B',
    activeRouteTab: '#00793A',
  
    /* Causes colors */
  
    causesEducation: '#097991',
    causesProfessionalTraining: '#a15b7f',
    causesPoverty: '#0090a3',
    causesConscientConsume: '#ff4e46',
    causesCultureArt: '#b8292b',
    causesHumanRights: '#b8292b',
    causesKids: '#ff9a58',
    causesOlders: '#a15b7f',
    causesEnviroment: '#00748d',
    causesCidadany: '#ff4e46',
    causesAnimals: '#0090a3',
    causesHealth: '#ff9a58',
    causesUnablePeople: '#ff9a58',
    causesPolitics: '#ff4e46',
    causesYouth: '#a15b7f',
    causesWomen: '#ff9e5e',
    causesSports: '#00748d',
    causesRefugies: '#00748d',
    causesLGBTQ: '#b8292b',
  
    /* Custom Colors */
    primaryBlue: '#0079c5',
    facebookBlue: '#3b5998',
    googleRed: '#dd4b39',
    deleteRed: '#d11a2a',
    lightBlue: '#bce0fd',
    darkBlue: '#0176ca',
    orangeDate: '#e26209',
    actuationCard: '#ededed',
    manageProjectRow: '#f8f9fa',
    dateHourInputBorder: '#cccccc',
    dateHourInputBackground: '#fafbfc',
    disabledBorder: '#c4e4fd',
  
    /* Modal Colors */
    modalColorBorder: '#777777',
    optionBorder: '#CCCCCC',
    optionBG: '#fafbfc'
  };
  
  export default COLORS;
  