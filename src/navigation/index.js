import React, { useEffect, useLayoutEffect, useState } from 'react';
import { Image } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import AsyncStorage from '@react-native-community/async-storage';
/* Screens */
import LoginScreen from '@screens/LoginScreen';
import EnterprisesScreen from '@screens/EnterprisesScreen';
import EnterpriseDetailsScreen from '@screens/EnterpriseDetailsScreen';

import userActions from '@actions/userActions';

import COLORS from '../config/colors';

/* Stacks */
const Stack = createStackNavigator();

const RootNavigator = () => {
  const dispatch = useDispatch();
  const user = useSelector(({ user }) => user.tokens);

  useEffect(() => {
    AsyncStorage.getItem('user', (_, result) => {
      if (result) {
        const user = JSON.parse(result);
        dispatch(userActions.USER_LOGIN_SUCCESS(user));
      }
    });
  }, []);

  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName={!!user ? 'Main' : 'Login'}
        screenOptions={{
          headerTitle: (
            <Image
              style={{ width: 50, height: 40 }}
              source={require('@assets/ioasys.png')}
            />
          ),
          headerTintColor: COLORS.white,
          headerStyle: {
            backgroundColor: COLORS.black,
            borderBottomColor: '#c5c5c5',
            borderBottomWidth: 1,
            borderBottomColor: COLORS.deepPink
          },
          headerTitleStyle: {
            color: '#fff',
            fontSize: 60,
            justifyContent: 'center',
            alignItems: 'center'
          },
          mode: Platform.OS === 'ios' ? 'modal' : 'card'
        }}
      >
        <Stack.Screen name='Login' component={LoginScreen} />
        <Stack.Screen name='Main' component={EnterprisesScreen} />
        <Stack.Screen name='Details' component={EnterpriseDetailsScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default RootNavigator;
