import React, { useState, useEffect } from 'react';
import { ScrollView, View, Text } from 'react-native';
import styled from 'styled-components';

import Loader from '@components/core/Loader';
import CONSTANTS from '@config/constants';
import COLORS from '@config/colors';

const EnterpriseDetails = ({
  enterprise,
  isLoading,
  enterpriseDetails,
  navigation,
  route
}) => {
  const { enterprise_id } = route.params;

  useEffect(() => {
    enterpriseDetails(enterprise_id);
  }, []);

  return (
    <Container>
      {isLoading ? (
        <Loader paddingTop={30} />
      ) : (
        <ScrollView>
          <ImageStyled
            source={{
              uri:
                'https://blog.pluga.co/uploads/2018/08/programa-de-administracao-para-empresas.png'
            }}
            // aspectRatio={1}
          />

          <ViewStyled>
            <Field>
              <TextLabel>Nome</TextLabel>
            </Field>
            <TextContent>{enterprise.enterprise_name}</TextContent>
          </ViewStyled>

          <ViewStyled>
            <Field>
              <TextLabel>Cidade</TextLabel>
            </Field>
            <TextContent>{enterprise.city}</TextContent>
          </ViewStyled>

          <ViewStyled>
            <Field>
              <TextLabel>País</TextLabel>
            </Field>
            <TextContent>{enterprise.country}</TextContent>
          </ViewStyled>

          <ViewStyled>
            <Field>
              <TextLabel>Categoria</TextLabel>
            </Field>
            <TextContent>
              {enterprise.enterprise_type?.enterprise_type_name}
            </TextContent>
          </ViewStyled>

          <ViewStyled>
            <Field>
              <TextLabel>Preço ação</TextLabel>
            </Field>
            <TextContent>{enterprise.share_price}</TextContent>
          </ViewStyled>

          <ViewStyled>
            <Field>
              <TextLabel>Descrição</TextLabel>
            </Field>
            <TextContent>{enterprise.description}</TextContent>
          </ViewStyled>
        </ScrollView>
      )}
    </Container>
  );
};

const ImageStyled = styled.Image`
  height: 300;
  margin-bottom: 15;
`;

const Container = styled.View`
  background-color: ${COLORS.white};
  flex: 1;
`;

const ViewStyled = styled.View`
  margin-left: 5;
  margin-right: 5;
  margin-top: 10;
  margin-bottom: 7;
  border-radius: 20;
  border-width: 1;
  border-color: ${COLORS.border};
  flex: 1;
`;

const Field = styled.View`
  position: relative;
  bottom: 15;
  left: 20;
`;

const TextLabel = styled.Text`
  position: absolute;
  font-size: 18;
  padding-left: 7;
  padding-right: 7;
  font-weight: bold;
  color: ${COLORS.deepPink};
  background-color: ${COLORS.white};
`;

const TextContent = styled.Text`
  font-size: 18;
  padding-left: 20;
  padding-right: 20;
  padding-top: 8;
  padding-bottom: 8;
`;

export default EnterpriseDetails;
