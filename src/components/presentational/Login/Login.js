import React, { useState, useEffect } from 'react';
import { Animated, Keyboard } from 'react-native';
import styled from 'styled-components';

import Loader from '@components/core/Loader';

import COLORS from '@config/colors';

const Login = props => {
  const { isLoading } = props;
  const [email, setEmail] = useState('testeapple@ioasys.com.br');
  const [password, setPassword] = useState('12341234');
  const [animationWidth] = useState(new Animated.Value(200));

  const signIn = () => {
    Keyboard.dismiss();
    const { signIn, navigation } = props;
    signIn(email, password, () => navigation.replace('Main'));
  };

  useEffect(() => {
    Animated.timing(animationWidth, {
      toValue: isLoading ? 40 : 200,
      duration: 500
    }).start();
  }, [isLoading]);

  return (
    <Container>
      <FormContainer>
        <ViewStyled>
          <FormRow>
            <Input
              placeholder={'email@email.com'}
              keyboardType='email-address'
              autoCapitalize='none'
              value={email}
              onChangeText={value => setEmail(value)}
            />
          </FormRow>
          <FormRow>
            <Input
              placeholder='*********'
              secureTextEntry
              value={password}
              onChangeText={value => setPassword(value)}
            />
          </FormRow>
          <ContainerButton>
            <StyledView width={animationWidth}>
              <ButtonStyled onPress={() => signIn()} activeOpacity={1.0}>
                {isLoading ? <Loader /> : <Label>Entrar</Label>}
              </ButtonStyled>
            </StyledView>
          </ContainerButton>
        </ViewStyled>
      </FormContainer>
    </Container>
  );
};

const Container = styled.View`
  flex: 1;
  background-color: ${COLORS.black};
`;

const FormContainer = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
`;

const ViewStyled = styled.View`
  background-color: ${COLORS.optionBG};
  border-radius: 10;
  width: 85%;
  height: 30%;
  border-width: 2;
  border-color: ${COLORS.deepPink};
`;

const FormRow = styled.View`
  padding-left: 10;
  padding-right: 10;
  padding-top: 10;
  background-color: ${COLORS.optionBG};
  margin-top: 5;
  margin-bottom: 5;
`;

const Input = styled.TextInput`
  justify-content: center;
  padding-left: 7;
  padding-right: 7;
  height: 40;
  border-radius: 6;
  border-width: 1;
  border-color: ${COLORS.black};
  background-color: ${COLORS.optionBG};
`;

const ContainerButton = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
`;

const ButtonStyled = styled.TouchableOpacity`
  background-color: ${COLORS.black};
  align-items: center;
  justify-content: center;
  height: 40;

  border-radius: 40;
`;

const Label = styled(Animated.Text)`
  font-size: 18;
  color: ${COLORS.white};
`;

const StyledView = styled(Animated.View)``;

export default Login;
