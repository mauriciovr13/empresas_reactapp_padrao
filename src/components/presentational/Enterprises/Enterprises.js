import React, { useState, useEffect, useLayoutEffect } from 'react';
import styled from 'styled-components';
import { FlatList, Button, View, Image } from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';

import { EnterpriseCard } from '@components/core/EnterpriseCard';
import Loader from '@components/core/Loader';
import ModalFilter from '@components/core/ModalFilter';

import COLORS from '@config/colors';
import STRINGS from '@config/strings';

const Enterprises = ({
  isLoading,
  enterprises,
  loadEnterprises,
  filterEnterprises,
  refreshing,
  logout,
  navigation
}) => {
  const [enterpriseType, setEnterpriseType] = useState(0);
  const [enterpriseName, setEnterpriseName] = useState('');
  const [modalVisible, setModalVisible] = useState(false);

  const logoutUser = () => {
    logout();
    navigation.reset({
      index: 0,
      routes: [{ name: 'Login' }]
    });
  };

  const filter = () => {
    if (enterpriseName.length === 0 && enterpriseType === 0)
      return loadEnterprises();

    filterEnterprises(enterpriseType, enterpriseName);
    setEnterpriseName('');
    setEnterpriseType(0);
  };

  const _keyExtractor = enterprise => enterprise.id.toString();

  const _renderItem = ({ item, index }) => (
    <EnterpriseCard
      enterprise={item}
      isFirst={index === 0}
      navigation={navigation}
    />
  );

  useEffect(loadEnterprises, []);

  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <TouchableOpacityStyled onPress={() => setModalVisible(!modalVisible)}>
          <Icon name='filter' size={30} color='#fff' />
        </TouchableOpacityStyled>
      )
    });
  }, [navigation]);

  return (
    <>
      <Container>
        {isLoading && enterprises.length === 0 ? (
          <Loader paddingTop={15} />
        ) : enterprises && enterprises.length > 0 ? (
          <FlatList
            data={enterprises}
            renderItem={_renderItem}
            keyExtractor={_keyExtractor}
            refreshing={refreshing}
            onRefresh={() => {
              loadEnterprises();
            }}
            ListFooterComponent={props => <FooterStyled />}
          />
        ) : (
          <StyledText>{STRINGS.homeScreen.notFoundEnterprises}</StyledText>
        )}
      </Container>
      <ModalFilter
        isOpen={modalVisible}
        toggleModal={() => setModalVisible(!modalVisible)}
        name={enterpriseName}
        setName={setEnterpriseName}
        type={enterpriseType}
        setType={setEnterpriseType}
        filterEnterprises={filter}
      />
      <ButtonLogout onPress={logoutUser} activeOpacity={0.5}>
        <Icon name='logout' size={25} color='#fff' />
      </ButtonLogout>
    </>
  );
};
const Container = styled.View``;

const TouchableOpacityStyled = styled.TouchableOpacity`
  justify-content: center;
  align-items: center;
  padding-left: 5;
  padding-right: 10;
`;

const FooterStyled = styled.View`
  padding-bottom: 20%;
`;

const StyledText = styled.Text`
  font-size: 16;
  color: ${COLORS.black};
  text-align: center;
  margin-top: 30;
`;

const ButtonLogout = styled.TouchableOpacity`
  position: absolute;
  width: 50;
  height: 50;
  bottom: 15;
  right: 15;
  background-color: ${COLORS.black};
  border-radius: 50;
  justify-content: center;
  align-items: center;
  border-width: 0.5;
  border-color: ${COLORS.deepPink};
`;

export default Enterprises;
