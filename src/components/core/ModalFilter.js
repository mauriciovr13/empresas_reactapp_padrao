import React from 'react';
import { TextInput, Button, Picker, Modal } from 'react-native';
import styled from 'styled-components';
import RNPickerSelect from 'react-native-picker-select';

import COLORS from '../../config/colors';

const ModalFilter = props => {
  const {
    isOpen,
    toggleModal,
    name,
    setName,
    type,
    setType,
    filterEnterprises
  } = props;

  return (
    <Modal
      animationType='slide'
      transparent
      visible={isOpen}
      onRequestClose={() => {}}
    >
      <StyledOverlay activeOpacity={1} onPress={toggleModal}>
        <StyledContent backgroundColor={COLORS.darkerBackground}>
          <Input
            placeholder='Enterprise name'
            value={name}
            onChangeText={setName}
          />
        </StyledContent>
        <StyledContent marginBottom={15}>
          <RNPickerSelect
            onValueChange={value => setType(value)}
            placeholder={{
              label: 'Select a category',
              value: null
            }}
            style={{
              inputAndroid: {
                paddingHorizontal: 10,
                paddingVertical: 20
              },
              placeholder: {
                color: '#B2B2B2'
              }
            }}
            value={type}
            items={[
              { label: 'Agro', value: '1' },
              { label: 'Aviation', value: '2' },
              { label: 'Biotech', value: '3' },
              { label: 'Eco', value: '4' },
              { label: 'Ecommerce', value: '5' },
              { label: 'Education', value: '6' },
              { label: 'Fashion', value: '7' },
              { label: 'Fintech', value: '8' },
              { label: 'Food', value: '9' },
              { label: 'Games', value: '10' },
              { label: 'Health', value: '11' },
              { label: 'IOT', value: '12' },
              { label: 'Logistics', value: '13' },
              { label: 'Media', value: '14' },
              { label: 'Mining', value: '15' },
              { label: 'Products', value: '16' },
              { label: 'Real Estate', value: '17' },
              { label: 'Service', value: '18' },
              { label: 'Smart City', value: '19' },
              { label: 'Social', value: '20' },
              { label: 'Software', value: '21' },
              { label: 'Technology', value: '22' },
              { label: 'Tourism', value: '23' },
              { label: 'Transport', value: '24' }
            ]}
          />
        </StyledContent>
        <StyledContent flexDirection='row'>
          <StyledTouchableItem onPress={toggleModal} width={400}>
            <Label>Cancelar</Label>
          </StyledTouchableItem>
          <StyledTouchableItem
            onPress={() => {
              filterEnterprises();
              toggleModal();
            }}
            width='50%'
          >
            <Label>Filtrar</Label>
          </StyledTouchableItem>
        </StyledContent>
      </StyledOverlay>
    </Modal>
  );
};

const StyledOverlay = styled.TouchableOpacity`
  flex: 1;
  justify-content: flex-end;
  align-self: stretch;
  padding-bottom: 5;
  background-color: 'rgba(0,0,0,0.5)';
`;

const StyledContent = styled.View`
  margin-left: 15;
  margin-right: 15;
  margin-bottom: ${({ marginBottom }) => marginBottom || 5};
  border-radius: 14;
  background-color: ${COLORS.white};
`;

const StyledTouchableItem = styled.TouchableOpacity`
  justify-content: center;
  align-items: center;
  padding-top: 20;
  padding-bottom: 20;
  width: 50%;
`;

const Input = styled.TextInput`
  margin-left: 5;
  margin-bottom: 5;
  margin-right: 15;
  margin-top: 5;
  font-size: 16;
  color: ${COLORS.secondaryText};
`;

const Label = styled.Text``;

export default ModalFilter;
