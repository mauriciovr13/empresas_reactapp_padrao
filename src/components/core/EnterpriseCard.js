import React from 'react';
import { Image } from 'react-native';
import styled from 'styled-components';

import CONSTANTS from '../../config/constants';
import COLORS from '../../config/colors';

export const EnterpriseCard = ({ enterprise, isFirst, navigation }) => {
  return (
    <StyledTouchableOpacity
      onPress={() =>
        navigation.navigate('Details', { enterprise_id: enterprise.id })
      }
      isFirst={isFirst}
      activeOpacity={0.7}
    >
      <Card>
        <Image
          style={{
            height: '100%'
          }}
          source={{
            uri:
              'https://blog.pluga.co/uploads/2018/08/programa-de-administracao-para-empresas.png'
          }}
          resizeMode='contain'
        />
        <CardTitleWraper>
          <CardTitle>{enterprise.enterprise_name}</CardTitle>
        </CardTitleWraper>
      </Card>
    </StyledTouchableOpacity>
  );
};

const StyledTouchableOpacity = styled.TouchableOpacity`
  height: 200;
  margin-top: ${({ isFirst }) => (isFirst ? 10 : 5)};
  margin-left: 10;
  margin-bottom: 5;
  margin-right: 10;
`;

const Card = styled.View`
  flex: 1;
  border-width: 1;
`;

const CardTitleWraper = styled.View`
  background-color: ${COLORS.black};
  height: 50;
  position: absolute;
  opacity: 1;
  width: 100%;
  justify-content: center;
  align-items: center;
  bottom: -1;
  border-top-color: ${COLORS.deepPink};
  border-top-width: 2;
`;

const CardTitle = styled.Text`
  color: ${COLORS.white};
  font-size: 15;
  font-weight: bold;
`;
