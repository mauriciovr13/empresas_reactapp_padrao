import React from 'react';
import styled from 'styled-components';
import COLORS from '@config/colors';

const Loader = props => {
  return (
    <Container {...props}>
      <ActivityIndicatorStyled size={'large'} color={COLORS.deepPink} />
    </Container>
  );
};

const Container = styled.View`
  justify-content: center;
  align-items: center;
`;

const ActivityIndicatorStyled = styled.ActivityIndicator``;

export default Loader;
